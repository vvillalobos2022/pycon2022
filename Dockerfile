FROM python:3.8.5-alpine
RUN apk add --no-cache alpine-sdk libffi-dev openssl-dev
RUN apk add bash
ENV PYTHONUNBUFFERED 1
RUN pip install --upgrade pip
RUN pip install -U pip && pip install gunicorn==19.9.0 && pip install falcon && pip install faker
RUN pip install ipdb
RUN apk add gcc musl-dev python3-dev libffi-dev openssl-dev cargo
RUN mkdir -p /code/src

WORKDIR /code


COPY ./src /code/src

RUN pip install -r src/layers/requirements.txt

WORKDIR /code


CMD [ "gunicorn", "api:create_app()", "--host=0.0.0.0", "--port=8080", "--reload" ]
