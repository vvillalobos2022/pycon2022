# -*- coding: utf-8 -*-
import pathlib
from enum import Enum

from fabric.api import env, local
from fabric.operations import prompt

STACK_NAME = "pycon-superheroes-service"


prompt(
    "Please specify an environment (dev or qe): ",
    key="aws_environ",
    validate=str,
    default="dev",
)
prompt(
    "please specify a db_name team-core for dev and playvox for qe: ",
    key="db_name",
    validate=str,
    default="team-core",
)
DB_NAME = env.db_name


local(f"echo -e 'Environment {env.aws_environ}'")


class Region(Enum):
    VIRGINIA = "us-east-1"  # for qe
    CALIFORNIA = "us-west-1"  # for dev


class Environment(Enum):
    qe = Region.VIRGINIA.value
    dev = Region.CALIFORNIA.value


env.aws_region = Environment[env.aws_environ].value
env.bucket_name = f"{env.aws_region}-pycon-superheroes"
env.path = pathlib.Path(__file__).parent.absolute()

env.environment = (
    Environment.qe.value
    if Region.VIRGINIA.value == env.aws_region
    else Environment.dev.value
)


def build_layer_packages():

    local("pip install -r src/layers/requirements.txt")
    local(
        "pip install --upgrade --requirement src/layers/requirements.txt \
           --target src/layers/python"
    )
    local("cd src/layers && zip -r layer-pycon-superheroes.zip ./python && cd - ")
    local("rm -rf src/layers/python")


def deploy():
    """
    This function makes the deploy of the pycon superheroes service
    """
    local("sam build")
    api_layer_version_command = f"aws lambda list-layer-versions --region {env.aws_region} --layer-name 'api-utils-layer' | jq '.LayerVersions[0].Version'"
    API_LAYER_VERSION = local(api_layer_version_command, capture=True)
    print("API layer", API_LAYER_VERSION)

    package = f"sam package --template-file .aws-sam/build/template.yaml --output-template-file \
                packaged.yaml --s3-bucket {env.bucket_name} --region {env.aws_region}"
    local(package)

    deploy = f"sam deploy --template-file packaged.yaml --stack-name {STACK_NAME} --parameter-overrides APILayerVersion={API_LAYER_VERSION} \
                env=prod DBName={DB_NAME} DBNamePlayvox={DB_NAME} --capabilities CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND --region {env.aws_region}"
    local(deploy)
