# -*- coding: utf-8 -*-
import json
import os
import uuid
from http.client import responses

import falcon
from bson import ObjectId
from integration_utils.utils import JSONEncoder

from src.marvel import controller


class Context(object):
    def __init__(
        self,
        aws_request_id=uuid.uuid4(),
        function_name="undefined",
        function_version="$LATEST",
        log_group_name="undefined",
        log_stream_name="undefined",
        invoked_function_arn="undefined",
        memory_limit_in_mb="0",
        client_context=None,
        identity=None,
    ):
        self.function_name = function_name
        self.function_version = function_version
        self.invoked_function_arn = invoked_function_arn
        self.memory_limit_in_mb = memory_limit_in_mb
        self.aws_request_id = aws_request_id
        self.log_group_name = log_group_name
        self.log_stream_name = log_stream_name
        self.identity = identity


context = Context()


class HealtCheck:
    def on_get(self, request, response):
        response.body = "pong"
        response.content_type = falcon.MEDIA_TEXT


class EmiterHandler:
    def on_post(self, request, response, request_context, superheroe_name):
        superheroes = [
            "{'name':'ironman','type':'marvel','power':'suit'}",
            "{'name':'hulk','type':'marvel','power':'force'}",
        ]
        superheroe = superheroes.find_one({"_id": ObjectId(superheroe_name)})
        sns_event = {
            "Records": [
                {
                    "EventSource": "aws:sns",
                    "EventVersion": "1.0",
                    "EventSubscriptionArn": "arn:aws:sns:us-east-1::ExampleTopic",
                    "Sns": {
                        "Type": "Notification",
                        "MessageId": "95df01b4-ee98-5cb9-9903-4c221d41eb5e",
                        "TopicArn": "arn:aws:sns:us-east-1:123456789012:ExampleTopic",
                        "Subject": "example subject",
                        "Message": json.dumps(
                            {
                                "superheroe": JSONEncoder().encode(superheroe),
                                "event_type": "sync_superheroes",
                            }
                        ),
                        "Timestamp": "1970-01-01T00:00:00.000Z",
                        "SignatureVersion": "1",
                        "Signature": "EXAMPLE",
                        "SigningCertUrl": "EXAMPLE",
                        "UnsubscribeUrl": "EXAMPLE",
                        "MessageAttributes": {
                            "superheroe_type": {
                                "DataType": "String",
                                "StringValue": superheroe["type"],
                            }
                        },
                    },
                }
            ]
        }
        resp = controller.main(sns_event, context)
        response.media = json.loads(resp["body"])
        response.status = f"{resp['statusCode']} {responses[resp['statusCode']]}"


def create_app():
    """
    Create the falcon app
    """
    app = falcon.App()

    app.add_route("/api/v1/ping", HealtCheck())
    app.add_route("/v1/marvel/emiter/{superheroe_name}", EmiterHandler())
    return app
