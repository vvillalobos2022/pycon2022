# -*- coding: utf-8 -*-
import json


def main(event, context):
    """
    this lambda receive events for the emiter filtered by Marvel
    """
    superheroes = get_superheroes(event)
    for superheroe in superheroes:
        print(superheroe)


def get_superheroes(event):
    records = event.get("Records", [])
    superheroes = []
    for record in records:
        message = json.loads(record.get("Sns", {}).get("Message"))
        superheroe = json.loads(message["superheroe"])
        superheroe["event_type"] = message["event_type"]
        superheroes.append(superheroe)
    return superheroes
