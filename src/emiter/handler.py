# -*- coding: utf-8 -*-
import json
import os

import boto3
from integration_utils.utils import IntegrationUtils, JSONEncoder, UnknowErrorException

SNS_INTEGRATIONS_TOPIC = os.environ.get("SNS_INTEGRATIONS_TOPIC")
boto_session = boto3.Session()
client_sns = boto_session.client(
    "sns", endpoint_url=os.environ.get("AWS_ENDPOINT_URL", None)
)


def main(event, context):
    try:
        superheroes = load_superheroes()
        for superheroe in superheroes:
            send_notification(superheroe)
        response = IntegrationUtils.success_response()
    except Exception as e:
        response = IntegrationUtils.handle_response(e)
    return response


def load_superheroes():
    """
    this method load all superheroes
    """
    return [
        {"name": "ironman", "type": "marvel", "power": "suit"},
        {"name": "hulk", "type": "marvel", "power": "force"},
        {"name": "superman", "type": "dc", "power": "fly"},
        {"name": "flash", "type": "dc", "power": "speed"},
    ]


def send_notification(superheroe):
    """
    send a sns notification with the superheroe information
    """
    try:
        message = {
            "superheroe": JSONEncoder().encode(superheroe),
            "event_type": "sync_superheroes",
        }

        message_attributes = {
            "superheroe_type": {
                "DataType": "String",
                "StringValue": superheroe["type"],
            }
        }

        response = client_sns.publish(
            TargetArn=SNS_INTEGRATIONS_TOPIC,
            Message=json.dumps(message),
            MessageAttributes=message_attributes,
        )
        return {"message_id": response["MessageId"], "message": message}
    except Exception as e:
        raise UnknowErrorException(f"Unable to send notification {e}")
