# -*- coding: utf-8 -*-
from integration_utils.utils import IntegrationUtils


def main(event, context):

    superheroes = [
        {
            "name": "ironman",
            "type": "marvel",
            "power": "suite",
        },
        {
            "name": "hulk",
            "type": "marvel",
            "power": "force",
        },
        {
            "name": "superman",
            "type": "dc",
            "power": "fly",
        },
        {
            "name": "flash",
            "type": "dc",
            "power": "speed",
        },
    ]

    return IntegrationUtils.success_response(data={"result": superheroes})
