# -*- coding: utf-8 -*-
import pymongo
from integration_utils import ssm_params


class DB:
    def __init__(self, ssm_param_name=None, database="playvox"):
        """
        ssm_param_name: name of the ssm parameter store to retrieve mongo
                        uri string
        database: name of the database
        """
        self._conn = None
        self.ssm_param_name = ssm_param_name
        self.database = database

    def get_conn(self):
        try:
            if not self._conn:
                if not self.ssm_param_name:
                    name = f"conf_{self.database}_db_uri"
                else:
                    name = self.ssm_param_name
                db_uri = ssm_params.get(name=name)
                client = pymongo.MongoClient(db_uri)
                self._conn = client[self.database]
            return self._conn
        except:
            raise Exception("Unable to connect to database")
