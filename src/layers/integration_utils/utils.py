# -*- coding: utf-8 -*-
import json
import logging
from datetime import datetime, timedelta
from uuid import uuid4

import jwt
import requests
from bson import ObjectId
from bson.errors import InvalidId
from cryptography.fernet import Fernet
from dateutil.parser import parse
from integration_utils import ssm_params
from requests.auth import HTTPBasicAuth

PV_BASE_URL = ssm_params.get(
    "/integrations/CustomIntegrations/PVBaseURL", with_decryption=False
)
CRYPTO_KEY = ssm_params.get(name="/integrations/CustomIntegrations/CryptoKey")
JWT_PRIVATE_KEY = ssm_params.get(name="/integrations/CustomIntegrations/PrivateKey")


class UnknowErrorException(Exception):
    pass


class DBException(Exception):
    pass


class MissingParentIdException(Exception):
    pass


class ResourceNotFoundException(Exception):
    pass


class GeneralBadRequestException(Exception):
    pass


class IntegrationMetadaUpsertFailed(Exception):
    pass


class Unauthorized(Exception):
    pass


class ForbiddenException(Exception):
    pass


class TokenException(Exception):
    pass


class CustomException(Exception):
    pass


class HandledException(Exception):
    pass


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime):
            return o.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        return json.JSONEncoder.default(self, o)


def custom_logging(feature=None, message=None, type_="error", exception=None, extra={}):
    """
    how to use:
    UtilsAPI.custom_logging(
        feature="export-center", message="Unable to export file", type_="error")
    :param feature: feature name, example: workloads
    :param type_: logging type info info, warning or error
    :param exception: (Exception): Exception related to the error
    :param extra: dict extra params to get more insights
    :returns: return the log message concatenated
    :raises N/A
    """
    try:
        feature_value = None
        if feature is not None:
            if isinstance(feature, str):
                feature_value = feature
            else:
                feature_value = feature.value
        log = f"feature:{feature_value},message:{message},extra:{extra}"
        if type_ == "info":
            logging.info(log)
        elif type_ == "warning":
            logging.warning(log)
        elif type_ == "error":
            if exception:
                log = f"{log} {exception}"
                if not isinstance(extra, dict):
                    extra = {}
                extra = {"data": str(extra)}
                logging.error(log, exc_info=True, extra=extra)
            else:
                logging.error(log)
        else:
            raise Exception(f"Invalid logging type {type_}")
    except Exception as e:
        log = f"custom_logging error type_ {e} is not valid"
        logging.error(log)
    return log


class IntegrationUtils:
    def __init__(self, db=None):
        self.db = db

    def get_connector(self, _id):
        # TODO: Enable rest call
        connector = self.db.connectors.find_one({"_id": ObjectId(_id)})
        if connector:
            try:
                credentials = self.get_decrypted_credentials(connector["credentials"])
                connector["credentials"] = credentials
                return connector
            except Exception:
                raise
        else:
            raise ResourceNotFoundException(f"Connector with {_id} not found")

    def update_connector_credentials(self, _id, new_credentials):
        connector = self.db.connectors.find_one({"_id": ObjectId(_id)})
        if connector:
            try:
                credentials = self.get_decrypted_credentials(connector["credentials"])
                credentials.update(new_credentials)
                crypted_credentials = self.get_crypted_credentials(credentials)
                self.db.connectors.update_one(
                    {"_id": connector["_id"]},
                    {"$set": {"credentials": crypted_credentials}},
                )
                connector["credentials"] = credentials
                return connector
            except:
                raise
        else:
            raise ResourceNotFoundException(f"Connector with {_id} not found")

    def get_decrypted_credentials(self, crypted_credentials):
        try:
            cypher = Fernet(CRYPTO_KEY)
            credentials = cypher.decrypt(crypted_credentials.encode("utf-8")).decode(
                "utf-8"
            )
            return json.loads(credentials)
        except Exception as e:
            raise GeneralBadRequestException(
                f"Unabele to decrypt connector credentials {e}"
            )

    def get_crypted_credentials(self, plain_credentials):
        try:
            cypher = Fernet(CRYPTO_KEY)
            credentials = cypher.encrypt(
                json.dumps(plain_credentials).encode("utf-8")
            ).decode("utf-8")
            return credentials
        except Exception as e:
            raise GeneralBadRequestException(
                f"Unabele to decrypt connector credentials {e}"
            )

    @staticmethod
    def handle_response(exception):
        if isinstance(exception, ResourceNotFoundException):
            status_code = 404
            error = {
                "message": "Resource not found",
                "code": "resource_not_found",
                "details": str(exception),
            }
        elif isinstance(exception, InvalidId):
            status_code = 400
            error = {
                "message": "Invalid id",
                "code": "invalid_id",
                "details": str(exception),
            }
        elif isinstance(exception, GeneralBadRequestException):
            status_code = 400
            error = {
                "message": "Bad request",
                "code": "bad_request",
                "details": str(exception),
            }
        elif isinstance(exception, IntegrationMetadaUpsertFailed):
            status_code = 400
            error = {
                "message": "Unable to create/update integration",
                "code": "upsert_integration_failed",
                "details": str(exception),
            }
        elif isinstance(exception, DBException):
            status_code = 500
            error = {"message": "Unknown error (DB)", "code": "unknown_error"}
        elif isinstance(exception, Unauthorized):
            status_code = 401
            error = {
                "message": "Unable to access resource",
                "code": "integration_unauthorized",
                "details": str(exception),
            }
        elif isinstance(exception, TokenException):
            status_code = 403
            error = {
                "message": "Unable to generate JWT",
                "code": "token_exception",
                "details": str(exception),
            }

        elif isinstance(exception, CustomException):
            status_code = exception.args[0]["status_code"]
            error = {
                "message": exception.args[0]["message"],
                "code": exception.args[0]["code"],
                "details": exception.args[0]["details"],
            }
        else:
            status_code = 500
            error = {
                "message": "Unknown error",
                "code": "unknown_error",
                "details": str(exception),
            }

        return IntegrationUtils.error_response(status_code=status_code, error=error)

    def make_playvox_request(
        self, site_id=None, resource=None, method=None, params=None, payload=None
    ):
        """
        Temp solution to make requests to Playvox.
        # Replace with the custom_integrations_request method
        """
        # TODO: Change the authorization
        token = IntegrationUtils.get_token(site_id)
        url = f"{PV_BASE_URL}/{resource}"
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"PLVX-JWT {token}",
        }
        response = requests.request(
            method,
            url,
            headers=headers,
            params=params,
            json=payload,
            verify=True,
            timeout=30,
        )
        return response

    @staticmethod
    def custom_integrations_request(
        site_id=None, resource=None, method=None, params=None, payload=None
    ):
        """
        Temp solution to make requests to Playvox.
        """
        # TODO: Change the authorization
        token = IntegrationUtils.get_token(site_id)
        url = f"{PV_BASE_URL}/{resource}"
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"PLVX-JWT {token}",
        }
        response = requests.request(
            method,
            url,
            headers=headers,
            params=params,
            json=payload,
            verify=True,
            timeout=30,
        )
        return response

    @staticmethod
    def get_token(site_id):
        expiration_in_seconds = 15 * 60
        expires_at = datetime.utcnow() + timedelta(seconds=expiration_in_seconds)
        payload = {
            "iss": "serverless",
            "aud": "playvox-legacy",
            "exp": expires_at,
            "sub": "m2m",
            "site_id": str(site_id),
            "jti": uuid4().hex,
            "iat": datetime.utcnow(),
        }
        try:
            token = jwt.encode(
                payload,
                JWT_PRIVATE_KEY,
                algorithm="RS256",
                headers={"module": "hybrid-integrations"},
            )
        except Exception as e:
            raise GeneralBadRequestException(f"Unable to generate JWT token ({e})")
        return token

    @staticmethod
    def check_for_new_allowed_values(field, value):
        """Check if there are new values for a specific field
        eg. new status is added on X integration and is not synced on Playvox

        Args:
            field (dict): metedata field from intetegration
            value (any): value to compare with the allowed field in the metadata

        Returns:
            list: the values not present in the allowed
        """
        new_values = []
        if field["allowed"] and value:
            if isinstance(value, list):
                new_values = list(set(value) - set(field["allowed"]))
            elif value not in field["allowed"]:
                new_values.append(value)
        return new_values

    @staticmethod
    def update_metadata_field(integration, field, new_values):
        """Update the allowed values for metadata field for an integrations

        Args:
            integration (dict): integration data
            field (dict): metedata field from intetegration to update
            new_values (list): the complete new values for the metadata field

        Raises:
            GeneralBadRequestException: [description]
        """
        allowed = field["allowed"] + new_values
        response = IntegrationUtils.custom_integrations_request(
            site_id=integration["site_id"],
            resource=f"api/v1/integrations/{integration['_id']}/metadata/{field['_id']}",
            method="PUT",
            payload={"allowed": allowed},
        )
        if response.status_code != 200:
            raise GeneralBadRequestException(
                f"Unable to update metadata for integration with id {integration['_id']}"
            )

    @staticmethod
    def send_interactions(integration, interactions):
        if interactions:
            response = IntegrationUtils.custom_integrations_request(
                site_id=integration["site_id"],
                resource=f"api/v1/integrations/{integration['_id']}/bulk/interactions",
                method="POST",
                payload=interactions,
            )
            if response.status_code != 201:
                raise GeneralBadRequestException(
                    f"Unable to send bulk interactions ({integration['_id']}) {response.json()}"
                )

    @staticmethod
    def update_last_sync(integration, last_synced_at):
        """
        Args:
            integration (dict): integration data
            last_synced_at (datetime): date

        Raises:
            GeneralBadRequestException: in case could't update last synced at date
        """
        response = IntegrationUtils.custom_integrations_request(
            site_id=integration["site_id"],
            resource=f"api/v1/integrations/{integration['_id']}",
            method="PUT",
            payload={"last_synced_at": IntegrationUtils.str_date(last_synced_at)},
        )
        if response.status_code != 200:
            raise GeneralBadRequestException(
                f"Unable to update last_synced_at ({integration['_id']})"
            )

    @staticmethod
    def str_date(date):
        if isinstance(date, datetime):
            return date.strftime("%Y-%m-%dT%H:%M:%SZ")

    @staticmethod
    def is_date(value):
        try:
            parse(value)
            return True
        except:
            return False

    @staticmethod
    def get_datetime_date(date):
        """return datetime giving a str

        Args:
            date (str): string date

        Returns:
            datetime: date converted to datetime
        """
        if IntegrationUtils.is_date(date):
            return parse(date, ignoretz=True)
        return None

    @staticmethod
    def get_last_synced_at(default_days=1, last_synced_date=None):
        """[summary]

        Args:
            default_days (int, optional): [description]. Defaults to 1.
            last_synced_date (datetime):

        Returns:
            datetime: last_synced date as datetime
        """
        if isinstance(last_synced_date, datetime):
            return last_synced_date
        elif isinstance(last_synced_date, str):
            return IntegrationUtils.get_datetime_date(last_synced_date)
        return datetime.utcnow() - timedelta(days=default_days)

    @staticmethod
    def success_response(status_code=200, data={}):
        """
        example response for lambda
        {
            "statusCode": 200,
            "body": json.dumps(response)
        }
        """
        body = {"success": True, **data}
        if "result" in body:
            body["result"] = json.loads(JSONEncoder().encode(body["result"]))
        response = {"statusCode": status_code, "body": json.dumps(body)}
        return response

    @staticmethod
    def error_response(status_code=500, error={}):
        body = {"success": False, "error": error}
        response = {"statusCode": status_code, "body": json.dumps(body)}
        return response
