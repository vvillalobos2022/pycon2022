# -*- coding: utf-8 -*-
import json
from abc import ABC, abstractmethod

from integration_utils.utils import (
    GeneralBadRequestException,
    IntegrationUtils,
    custom_logging,
)


class BaseHandler(ABC):
    def __init__(self, db=None, event=None):
        self.db = db
        self.event = event

    @abstractmethod
    def get_instance(self, instance_type):
        pass

    def start(self):
        utils = IntegrationUtils(db=self.db)
        path_parameters = self.event.get("pathParameters") or {}
        option = path_parameters.get("option")
        connector_id = path_parameters.get("connector_id")
        if not option:
            option = self.event["path"].split("/")[-1]
        try:
            if option == "settings":
                connector = utils.get_connector(_id=connector_id)
                response = self.get_settings(connector)
            elif option == "details":
                connector = utils.get_connector(_id=connector_id)
                response = self.get_details(connector)
            elif option == "raw-data":
                connector = utils.get_connector(_id=connector_id)
                response = self.get_raw_data(connector)
            elif option == "validate":
                response = self.validate()
            elif option == "form":
                response = self.form()
            else:
                raise GeneralBadRequestException("Invalid option")
        except Exception as e:
            response = IntegrationUtils.handle_response(e)
        return response

    def get_settings(self, connector):
        try:
            workspace_id = None
            if self.event["queryStringParameters"]:
                workspace_id = self.event["queryStringParameters"].get("workspace_id")
            settings = self.get_instance("settings")(connector, self.db)
            if workspace_id:
                result = settings.get_workspace(workspace_id)
            else:
                result = settings.get_workspaces()
            return IntegrationUtils.success_response(data={"result": result})
        except Exception as e:
            custom_logging(
                feature=connector["type"],
                message=f"Handler settings exception for connector with id {connector['_id']}",
                exception=e,
            )
            raise

    def validate(self):
        try:
            credentials = json.loads(self.event["body"]).get("credentials")
            if credentials:
                interaction = self.get_instance("validate")(credentials)
                return IntegrationUtils.success_response(
                    data={"result": interaction.validate()}
                )
            else:
                raise GeneralBadRequestException("Invalid credentials")
        except Exception as e:
            raise e

    def get_details(self, connector):
        try:
            external_id = self.event["pathParameters"].get("external_id")
            if external_id:
                interaction = self.get_instance("details")(
                    connector=connector, db=self.db, external_id=external_id
                )
                return IntegrationUtils.success_response(
                    data={"result": interaction.get_details()}
                )
            else:
                raise GeneralBadRequestException("External id is required")
        except Exception as e:
            custom_logging(
                feature=connector["type"],
                message=f"Handler details exception for connector with id {connector['_id']}",
                exception=e,
            )
            raise

    def get_raw_data(self, connector):
        try:
            external_id = self.event["pathParameters"].get("external_id")
            if external_id:
                interaction = self.get_instance("raw-data")(
                    connector=connector, db=self.db, external_id=external_id
                )
                return IntegrationUtils.success_response(
                    data={"result": interaction.get_raw_data()}
                )
            else:
                raise GeneralBadRequestException("External id is required")
        except Exception as e:
            custom_logging(
                feature=connector["type"],
                message=f"Handler raw_data exception for connector with id {connector['_id']}",
                exception=e,
            )
            raise

    def form(self):
        try:
            form_fields = self.get_instance("form")()
            return IntegrationUtils.success_response(
                data={"result": form_fields.form_fields()}
            )
        except Exception as e:
            raise e


class SettingsBase(ABC):
    @abstractmethod
    def get_workspaces(self):
        pass

    @abstractmethod
    def get_workspace(self, workspace_id):
        pass


class DetailsBase(ABC):
    @abstractmethod
    def get_details(self):
        pass
