# -*- coding: utf-8 -*-
import functools
import json
import pprint
import signal
from http import HTTPStatus

import jwt
from integration_utils.utils import custom_logging

TIME_PERCENT = 10


def lambda_logger():
    def decorator(handler):
        @functools.wraps(handler)
        def data_dog_logger(event: dict, context, *args, **kwargs):
            response: dict = {}

            def timeout_handler(_signal, _frame):
                try:
                    error_label = f"lambdaLog:function_name={context.function_name}:function_version={context.function_version}:invoked_function_arn={context.invoked_function_arn}"
                    if event.get("headers", {}).get("Authorization"):
                        del event["headers"]["Authorization"]
                    if event.get("multiValueHeaders", {}).get("Authorization"):
                        del event["multiValueHeaders"]["Authorization"]

                    data = {
                        "timeout": timeout,
                        "manager_timeout": timeout_in_second,
                        "event": event,
                        "context": vars(context),
                    }
                    custom_logging(
                        feature=error_label,
                        message=pprint.pformat(data),
                    )
                except Exception as e:
                    custom_logging(
                        feature="lambdaLog",
                        message="Uncontroller exception",
                        exception=e,
                    )
                raise TimeoutError(error_label)

            signal.signal(signal.SIGALRM, timeout_handler)
            timeout: int = context.get_remaining_time_in_millis() / 1000
            timeout_in_second: int = round(timeout * TIME_PERCENT / 100)
            timeout_in_second = timeout - (
                10 if timeout_in_second > 10 else timeout_in_second
            )
            timeout_in_second = timeout if timeout_in_second <= 0 else timeout_in_second
            signal.alarm(int(timeout_in_second))
            response = handler(event, context, *args, **kwargs)
            signal.alarm(0)
            return response

        return data_dog_logger

    return decorator
