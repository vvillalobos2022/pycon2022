# -*- coding: utf-8 -*-
import json
from abc import ABC, abstractmethod

from integration_utils.utils import (
    HandledException,
    IntegrationUtils,
    Unauthorized,
    custom_logging,
)


class BaseController(ABC):
    def __init__(self, db=None, event=None):
        self.db = db
        self.event = event

    def start(self):
        utils = IntegrationUtils(db=self.db)
        integrations = self.get_integrations()
        total = 0
        for integration in integrations:
            try:
                connector = utils.get_connector(integration["connector_id"])
                worker = self.get_worker(integration["event_type"])
                worker(connector=connector, integration=integration, db=self.db).start()
                total += 1
            except HandledException as e:
                pass

            except Unauthorized as e:
                custom_logging(
                    feature=integration["type"],
                    type_="warning",
                    message=f"Controller exception for connector with id {integration['connector_id']}",
                    extra=e,
                )

            except Exception as e:
                custom_logging(
                    feature=integration["type"],
                    message=f"Controller exception for connector with id {integration['connector_id']}",
                    exception=e,
                )
        return IntegrationUtils.success_response(data={"result": {"total": total}})

    def get_integrations(self):
        records = self.event.get("Records", [])
        integrations = []
        for record in records:
            message = json.loads(record.get("Sns", {}).get("Message"))
            integration = json.loads(message["integration"])
            integration["event_type"] = message["event_type"]
            integrations.append(integration)
        return integrations

    @abstractmethod
    def get_worker(self, sync_type):
        pass


class SyncerBase(ABC):
    @abstractmethod
    def start(self):
        pass
