# -*- coding: utf-8 -*-
import os

import boto3


def get(name=None, with_decryption=True):
    if os.environ.get("PV_SERVERLESS_MODE") == "dev":
        try:
            # for ssm path parameters eg. /ExportCenter/DBUrlString
            name = name.replace("/", "_")
            return os.environ[name]
        except:
            raise Exception(f"Enviroment variable {name} not found")
    else:
        try:
            ssm = boto3.client("ssm")
            parameter = ssm.get_parameter(Name=name, WithDecryption=with_decryption)
            return parameter["Parameter"]["Value"]
        except:
            raise Exception("Unable to get SSM parameter")
